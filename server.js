//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

// var urlMLab = "https://api.mlab.com/api/1/databases/cmoreno/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMLab = "https://api.mlab.com/api/1/databases/cmoreno/collections/";

var paramApiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res) {
  //res.send("Hola API NodeJS");
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/', function(req, res){
  res.send("Peticion POST recibida");
});


app.put('/', function(req, res){
  res.send("Hemos recibido su peticion PUT modificada");
});

app.delete('/', function(req, res){
  res.send("Peticion DELETE recibida");
});

/*
app.get('/clientes/:idCliente/:nombre',function(req, res) {
  res.send("Cliente con id: " + req.params.idCliente + " Con nombre: " + req.params.nombre);

});
*/
/*
app.get('/clientes/:idCliente/',function(req, res) {
  res.send("Cliente con id: " + req.params.idCliente);

});

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req, res){
  //res.sendFile(path.join(__dirname,'movimientosv1.json'));
  res.send(movimientosJSON);
});
*/


/*METODOS PARA EL PROYECTO FINAL*/

//Metodo GET para la consulta de cuentas
app.get('/cuentas',function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"cuentas?"+paramApiKey);
  clienteMLab.get('',function(err,resM, body){
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
});


//Metodo GET para la consulta de cuentas por cliente
app.get('/cuentas/:idcliente',function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"cuentas?"+paramApiKey+"&q={\"idcliente\":"+req.params.idcliente+"}");
  clienteMLab.get('',function(err,resM, body){
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
});


// //Metodo PUT para adicionar movimientos a una cuenta
// app.put('/cuentas/:idCliente:idAccount',function(req,res){
//   var clienteMLab = requestjson.createClient(urlMLab+"cuentas?"+paramApiKey+"&q={\"idcliente\":"+req.params.idCliente+"}");
//   clienteMLab.get('',function(err,resM, body){
//     if (err) {
//       console.log(body);
//     }else {
//       res.send(body);
//     }
//   })
// });

//Metodo POST para la creacion o insert de nuevas cuentas
app.post('/cuentas', function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"cuentas?"+paramApiKey);
  clienteMLab.post('',req.body, function(err,resM, body){
    res.send(resM.statusCode);
  })
});


//Metodo GET para la consulta de usuarios
app.get('/usuarios',function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"usuarios?"+paramApiKey);
  clienteMLab.get('',function(err,resM, body){
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
});


//Metodo GET para la consulta de usuarios por IdCliente
app.get('/usuarios/:idcliente',function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"usuarios?"+paramApiKey+"&q={\"idcliente\":"+req.params.idcliente+"}");
  clienteMLab.get('',function(err,resM, body){
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
});

//Metodo POST para la creacion o insert de nuevos usuarios
app.post('/usuarios', function(req,res){
  var clienteMLab = requestjson.createClient(urlMLab+"usuarios?"+paramApiKey);
  clienteMLab.post('',req.body, function(err,resM, body){
    res.send(resM.statusCode);
  })
});
